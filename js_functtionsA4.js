=>Task #1

function isEquals(x, y) {
  return x == y;
}
console.log(isEquals(3,3));

=>Task #2
function isBigger(x, y) {
  return x > y;
}
console.log(isBigger(5,-1));

=>Task #3
function storeNames(...names) {
  return names;
}
console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy')); 

=>Task #4
function getDifference(x, y) {
  return x - y;
}
console.log(getDifference(5,-1));

=>Task #5
function negativeCount(...args) {
    let c=0;
  for(let i of args){
      if(i<0)
        c+=1;
  }
  return c;  
}
console.log(negativeCount(4, -3, 2, -9));

=>Task #6
function letterCount(x,y) {
    let c=0;
  for(let i of x){
      if(i==y)
        c+=1;
  }
  return c;  
}
console.log(letterCount("Barny", "y"));

=>Task #7
function countPoints(scores) {
  let points = 0;
  for (let i = 0; i < scores.length; i++) {
    const [x, y] = scores[i].split(':').map(Number);
    if (x > y) {
      points += 3;
    } else if (x === y) {
      points += 1;
    }
  }
  return points;
}
console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));

